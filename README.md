---
title: Analyse de réseau sur le corpuce Marilyn
author:
    - Nicolas Sauret, Servanne Monjour
date: 2024-02-14
licence: CC-BY-SA
---

## Analyse de réseau sur le corpuce Marilyn

Ce répertoire contient des visualisations de différents réseaux (hashtags, profils) issus du [corpuce Marilyn](https://gitlab.huma-num.fr/corpuces/projet-marilyn/corpuce-marilyn), produites avec [twitter explorer](https://github.com/pournaki/twitter-explorer/) du MediaLab de Sciences Po :

- [Réseau des hashtags](https://corpuces.gitpages.huma-num.fr/projet-marilyn/analyse-reseau/all.twitwi__nt0_lt0_HTN.html)
- [Réseau des mentions twitter](https://corpuces.gitpages.huma-num.fr/projet-marilyn/analyse-reseau/all.twitwi__mentionnetwork.html)
- [Réseau des profils mentionnés](https://corpuces.gitpages.huma-num.fr/projet-marilyn/analyse-reseau/all.twitwi__quotenetwork.html)
- [Réseau des profils répondus](https://corpuces.gitpages.huma-num.fr/projet-marilyn/analyse-reseau/all.twitwi__replynetwork.html)

---

![CC-BY-SA 4.0](http://i.creativecommons.org/l/by-sa/3.0/80x15.png)
Ces travaux sont sous licence [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
